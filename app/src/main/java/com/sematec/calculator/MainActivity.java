package com.sematec.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    TextView txtSum;
    EditText num1;
    EditText num2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("ماشین حساب");
        findViewById(R.id.btnSum).setOnClickListener(this);
        findViewById(R.id.btnSubtract).setOnClickListener(this);
        findViewById(R.id.btnMultiplication).setOnClickListener(this);
        findViewById(R.id.btnDivide).setOnClickListener(this);
        Bind();
        num1 = findViewById(R.id.edit_text_num1);


    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnSum) {
            int x = Integer.parseInt(num1.getText().toString());
            int y = Integer.parseInt(num2.getText().toString());
            int res = x+y;
            txtSum.setText(Integer.toString(res));

        }
        if (view.getId() == R.id.btnSubtract) {
            int x = Integer.parseInt(num1.getText().toString());
            int y = Integer.parseInt(num2.getText().toString());
            int res = x-y;
            txtSum.setText(Integer.toString(res));

        }
        if (view.getId() == R.id.btnMultiplication) {
            int x = Integer.parseInt(num1.getText().toString());
            int y = Integer.parseInt(num2.getText().toString());
            int res = x*y;
            txtSum.setText(Integer.toString(res));

        }
        if (view.getId() == R.id.btnDivide) {
            int x = Integer.parseInt(num1.getText().toString());
            int y = Integer.parseInt(num2.getText().toString());
            int res = x/y;
            txtSum.setText(Integer.toString(res));

        }


    }

    void Bind() {
        txtSum = findViewById(R.id.TxtSum);

        num2 = findViewById(R.id.edit_text_num2);

    }



}
